import { Placemark } from './map.component';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';

@Injectable({
  providedIn: 'root'
})
export class MapService {

  constructor(private httpClient: HttpClient) { }

  getPlaceMarks():  Promise<Placemark[]> {
    return this.httpClient.get(environment.baseUrl + '/api/v1/cars')
      .toPromise()
      .then(response => response['placemarks'] as Placemark[])
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('Error Connecting To Backend', error);
    return Promise.reject(error.message || error);
  }

}
