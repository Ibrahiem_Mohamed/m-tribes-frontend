import { Component, OnInit } from "@angular/core";
import { MapService } from './map.service';

@Component({
  selector: "app-map",
  templateUrl: "./map.component.html",
  styleUrls: ["./map.component.css"]
})
export class MapComponent implements OnInit {
  // initial zoom
  zoom = 11;
  // initial center position for the map
  lat = 53.59301;
  lng = 10.07526;
  placeMarks: Placemark[] = [];

  constructor(private mapService: MapService) {}

  ngOnInit(): void {
    this.getPlaceMarks();
  }

  getPlaceMarks(): void {
    this.mapService.getPlaceMarks()
    .then(placeMarks => (this.placeMarks = placeMarks) )
    .catch(error => alert('Erro Connecting to backend please try again later')); // to be handeld with popup
  }
}

export interface Placemark {
  address: string;
  coordinates: number[];
  engineType: string;
  exterior: string;
  fuel: number;
  interior: string;
  name: string;
  vin: string;
}
